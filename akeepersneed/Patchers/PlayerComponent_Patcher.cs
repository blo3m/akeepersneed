﻿using System;
using Harmony;
using UnityEngine;

namespace AKeepersNeed
{
    [HarmonyPatch(typeof(PlayerComponent))]
    [HarmonyPatch("Update")]
    internal class PlayerComponent_Update_Patcher
    {
        [HarmonyPrefix]
        public static bool Prefix(PlayerComponent __instance)
        {
            string[] options = Config.GetOptions();
            if (Convert.ToBoolean(Config.getChachedOption("iEnergy")) == true)
            {
                //Credit: oldark1
                MainGame.me.player.energy = (float)MainGame.me.save.max_energy - 1;
            }
            if(Convert.ToBoolean(Config.getChachedOption("iHp")) == true)
            {
                MainGame.me.player.hp = (float)MainGame.me.save.max_hp;
            }
            return true;
        }
    }

    
}
