﻿using System;
using Harmony;

namespace AKeepersNeed
{
    [HarmonyPatch(typeof(TechPointsSpawner))]
    [HarmonyPatch("Spawn")]
    class TechPointsSpawner_Spawn_Patch
    {
        public static int multiplier = Convert.ToInt32(Config.getChachedOption("techMul"));

        [HarmonyPrefix]
        public static void Postfix(TechPointsSpawner __instance, ref int r, ref int g, ref int b)
        {
            if(r > 0)
            {
                r = r * multiplier;
            }
            if(g > 0)
            {
                g = g * multiplier;
            }
            if(b > 0)
            {
                b = b * multiplier;
            }
            
        }
    }
}
