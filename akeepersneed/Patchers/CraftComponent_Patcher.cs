﻿using System;
using Harmony;
using UnityEngine;

namespace AKeepersNeed
{
    //credit: oldark1
    [HarmonyPatch(typeof(CraftComponent))]
    [HarmonyPatch("DoAction")]
    class CraftComponent_DoAction_Patch
    {
        [HarmonyPrefix]
        static bool Prefix(CraftComponent __instance, ref WorldGameObject other_obj, ref float delta_time)
        {
            if (__instance.current_craft.is_auto)
                delta_time *= (float)Convert.ToDouble(Config.getChachedOption("wSpeed"));
            if (!(__instance.current_craft.is_auto))
                delta_time *= (float)Convert.ToDouble(Config.getChachedOption("wSpeed"));
            return true;
        }
    }
}
