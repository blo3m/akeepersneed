﻿using System;
using Harmony;
using UnityEngine;

namespace AKeepersNeed
{
    //credit: oldark1
    [HarmonyPatch(typeof(MovementComponent))]
    [HarmonyPatch("UpdateMovement")]
    internal class MovementComponent_UpdateMovement_Patch
    {
        [HarmonyPostfix]
        public static void Postfix(MovementComponent __instance)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (__instance.wgo.is_player)
                {
                    __instance.SetSpeed((float)Convert.ToDouble(Config.getChachedOption("Speed")));
                    MainGame.me.player.energy -= (float)Convert.ToDouble(Config.getChachedOption("Energy"));
                }
            }
            if(!Input.GetKey(KeyCode.LeftShift))
            {
                if (__instance.wgo.is_player)
                    __instance.SetSpeed(3.0f);
            }
        }
    }

}
