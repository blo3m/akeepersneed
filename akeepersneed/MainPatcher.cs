﻿using System.IO;
using Harmony;
using System.Reflection;

namespace AKeepersNeed
{
    //Credits: oldark1
    public class MainPatcher
    {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.blommm.graveyardkeeper.akeepersneed.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    public class Config
    {  
        public static string path = @"./QMods/AKeepersNeed/config.txt";
        public static string speed, energy, ienergy, ihp, wspeed, techmul, sellmul;

        public static string[] GetOptions()
        {
            //Credits: oldark1  
            string[] options;
            StreamReader file = new StreamReader(path);
            string line = file.ReadToEnd();
            line.Split(',');
            options = line.Split(',', '=');
            return options;
        }

        public static string getChachedOption(string option)
        {
            string[] options = GetOptions();
            if (option == "Speed")
            {
                if(speed == null)
                    speed = options[1];
                return speed;
            }
            else if (option == "Energy")
            {
                if (energy == null)
                    energy = options[3];
                return energy;
            }
            else if (option == "iEnergy")
            {
                if (ienergy == null)
                    ienergy = options[5];
                return ienergy;
            }
            else if (option == "iHp")
            {
                if (ihp == null)
                    ihp = options[7];
                return ihp;
            }
            else if (option == "wSpeed")
            {
                if (wspeed == null)
                    wspeed = options[9];
                return wspeed;
            }
            else if (option == "techMul")
            {
                if (techmul == null)
                    techmul = options[11];
                return techmul;
            }
            else if (option == "sellMul")
            {
                if (techmul == null)
                    techmul = options[13];
                return techmul;
            }
            else
                return null;
        }
    }
}
